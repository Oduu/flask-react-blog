from resources.models.base import Base
import sqlalchemy as sa
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

class Comment(Base):
	__tablename__ = 'comment'
	id = sa.Column(sa.String(36), primary_key=True)
	post_id = sa.Column(sa.String(72), sa.ForeignKey("post.id"))
	body = sa.Column(sa.String(72))
	author = sa.Column(sa.String(72))
	# for migrate test later
	# author = sa.Column(sa.Integer)

class CommentSchema(SQLAlchemySchema):
    class Meta:
        model = Comment
        fields = ("id", "post_id", "body", "author")
        # load_instance = True
