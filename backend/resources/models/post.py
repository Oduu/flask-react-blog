from resources.models.base import Base
import sqlalchemy as sa
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

class Post(Base):
	__tablename__ = 'post'
	id = sa.Column(sa.String(36), primary_key=True)
	title = sa.Column(sa.String(72))
	body = sa.Column(sa.String(72))
	author = sa.Column(sa.String(72))

class PostSchema(SQLAlchemySchema):
    class Meta:
        model = Post
        fields = ("id", "title", "body", "author")
        # load_instance = True