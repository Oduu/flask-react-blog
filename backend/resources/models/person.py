from resources.models.base import Base
import sqlalchemy as sa
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

class Person(Base):
	__tablename__ = 'person'
	id = sa.Column(sa.String(36), primary_key=True)
	username = sa.Column(sa.String(45), unique=True)
	password = sa.Column(sa.String(72))

class PersonSchema(SQLAlchemySchema):
    class Meta:
        model = Person
        fields = ("id", "username", "password")
        # load_instance = True

	    