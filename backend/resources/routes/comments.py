from flask_restful import Api, Resource, reqparse, abort
from resources.repo.post import PostRepo
from resources.repo.comment import CommentRepo
from flask import session, request
import uuid

post_comments = reqparse.RequestParser()

post_comments.add_argument("post_id", type=str, help="Please send a comment id", required=True)
post_comments.add_argument("body", type=str, help="Please send a comment body", required=True)

def abort_on_no_exist(comment_id):
	if not CommentRepo().get(comment_id):
		abort(404, error="Could not find comment_id with given id")

class Comments(Resource):
	def get(self, comment_id):
		abort_on_no_exist(comment_id)
		return CommentRepo().get(comment_id)

	def post(self):
		# check_login()
		args = post_comments.parse_args()
		comment_id = str( uuid.uuid4())
		return CommentRepo().insert(comment_id, args["post_id"], args["body"], session["username"])