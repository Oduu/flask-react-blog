from flask_restful import Api, Resource, reqparse, abort
from resources.repo.person import PersonRepo
from flask import session, request
import bcrypt

session_args = reqparse.RequestParser()

# with required True, if not provided the help message will be returned instead of setting the value to null
session_args.add_argument("username", type=str, help="Please send a username", required=True)
session_args.add_argument("password", type=str, help="Please send a password", required=True)

class Sessions(Resource):

	# login/auth

	# needs to be seperated into login and check session
		# login needs to remove any current session
		# check session just check
	def post(self):

		if "username" in session:
			return session["username"]
		else:
			args = session_args.parse_args()
			text_username = str(args["username"])
			text_password = str(args["password"])

			exists = PersonRepo().get_with_password(text_username)

			if exists and bcrypt.checkpw(text_password.encode("utf-8"), exists['password'].encode('utf-8')):
				session["user_id"] = exists["id"]
				session["username"] = exists["username"]

				return exists, 200

			else:
				return { "error": "Incorrect credentials provided"},401

			

		# return { "username": text_username, "password": text_password }

	def delete(self):
		session.pop('username', None)
		session.pop('user_id', None)
		return { "message": "deleted" }, 204