from flask_restful import Api, Resource, reqparse, abort
from flask import session
import bcrypt
import uuid
from resources.repo.person import PersonRepo
from resources.models.person import Person, PersonSchema
from config import dbsession

post_user_args = reqparse.RequestParser()
post_user_args.add_argument("username", type=str, help="Please send a username", required=True)
post_user_args.add_argument("password", type=str, help="Please send a password", required=True)

get_user_args = reqparse.RequestParser()
get_user_args.add_argument("username", type=str, help="Please send a username", required=True)

def exists(username):
	db_check = PersonRepo().get(username)
	if db_check:
		return db_check
	else:
		abort(404, error="Could not find user with given username")


class Users(Resource):
	# get user details
	def get(self, username):
		return exists(username)

	# register new user
	def post(self):

		args = post_user_args.parse_args()

		exists = PersonRepo().get(args["username"])

		if exists:
			return {"error": "Username already taken."}, 409
		else:
			person_id = str( uuid.uuid4())
			password = bcrypt.hashpw(args["password"].encode("utf-8"), bcrypt.gensalt()).decode("utf-8")

			newUser = PersonRepo().insert(person_id, args["username"], password)

			# flask session assignments
			session["user_id"] = newUser["id"]
			session["username"] = newUser["username"]

			return self.get(newUser["username"])
