from flask_restful import Api, Resource, reqparse, abort
from resources.repo.post import PostRepo
from flask import session, request
import uuid

blog_put_args = reqparse.RequestParser()

# with required True, if not provided the help message will be returned instead of setting the value to null
blog_put_args.add_argument("title", type=str, help="Please send a post title", required=True)
blog_put_args.add_argument("body", type=str, help="Please send a post body", required=True)


def abort_on_no_exist(post_id):
	if not PostRepo().get(post_id):
		abort(404, error="Could not find post with given id")

def abort_on_exist(post_id):
	if PostRepo().get(post_id):
		abort(409, error="Post already exists with that id")

def check_login():
	if "user_id" not in session:
		abort(401, error="Not signed in.")

def compare_user(author):
	if session["username"] != author:
		abort(401, error="You do not have access to this record.")


class Posts(Resource):
	def get(self, post_id):
		abort_on_no_exist(post_id)
		return PostRepo().get(post_id)

	def post(self):
		args = blog_put_args.parse_args()
		check_login()
		post_id = str( uuid.uuid4())
		return PostRepo().insert(post_id, args["title"], args["body"], session["username"])

	def delete(self, post_id):
		check_login()
		abort_on_no_exist(post_id)
		author = self.get(post_id)["author"]
		compare_user(author)
		PostRepo().delete(post_id)
		return '', 204





