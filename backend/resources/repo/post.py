from resources.models.post import Post, PostSchema
from config import dbsession

class PostRepo():
	def get(self, post_id):
		query = dbsession.query(Post).filter_by(id=post_id).first()
		result = self.serialize(query)
		return result

	def insert(self, post_id, title, body, username):
		newPost = Post(id=post_id, title=title, body=body, author=username)
		dbsession.add(newPost)
		dbsession.commit()
		return self.get(post_id)

	def delete(self, post_id):
		alc_obj = dbsession.query(Post).filter_by(id=post_id).first()
		dbsession.delete(alc_obj)
		dbsession.commit()
		return "success"

	def serialize(self, alc_obj):
		schema = PostSchema()
		result = schema.dump(alc_obj)
		return result
