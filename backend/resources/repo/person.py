from resources.models.person import Person, PersonSchema
from config import dbsession

class PersonRepo():
	def get(self, username):
		query = dbsession.query(Person.id, Person.username).filter_by(username=username).first()
		result = self.serialize(query)
		return result

	def get_with_password(self, username):
		query = dbsession.query(Person).filter_by(username=username).first()
		result = self.serialize(query)
		return result

	def insert(self, person_id, username, password):
		newUser = Person(id=person_id, username=username, password=password)
		dbsession.add(newUser)
		dbsession.commit()
		return self.get(username)

	def serialize(self, alc_obj):
		schema = PersonSchema()
		result = schema.dump(alc_obj)
		return result
