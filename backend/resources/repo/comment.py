from resources.models.comment import Comment, CommentSchema
from config import dbsession

class CommentRepo():
	def get(self, comment_id):
		query = dbsession.query(Comment).filter_by(id=comment_id).first()
		result = self.serialize(query)
		return result

	def insert(self, comment_id, post_id, body, author):
		newComment = Comment(id=comment_id, post_id=post_id, body=body, author=author)
		dbsession.add(newComment)
		dbsession.commit()
		return self.get(comment_id)


	def serialize(self, alc_obj):
		schema = CommentSchema()
		result = schema.dump(alc_obj)
		return result
