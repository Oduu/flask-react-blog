from resources.repo.conn import DB


class MySQLRepo():

	def __init__(self):
		self.con = DB.mysql()
		self.cursor = self.con.cursor()


class SQLPersonRepo(MySQLRepo):

	def get(self, username):
		query = """
			SELECT id, username, password FROM person WHERE username = %s;
		"""

		self.cursor.execute(query,(username,))
		return self.cursor.fetchone()

	def get_no_pass(self, username):
		query = """
			SELECT id, username FROM person WHERE username = %s;
		"""

		self.cursor.execute(query,(username,))
		return self.cursor.fetchone()

	def insert(self, person_id, username, password):
		query = """
			INSERT INTO person(id, username, password) VALUES(%s, %s, %s);
		"""
		self.cursor.execute(query, (person_id, username, password))

		self.con.commit()

		return self.get(username)



class SQLPostRepo(MySQLRepo):

	def get(self, post_id):
		query = """
			SELECT id, title, body, author FROM post where id = %s;
		"""

		self.cursor.execute(query,(post_id,))
		return self.cursor.fetchall()

	def insert(self, post_id, title, body, author):
		self.cursor.execute(""" INSERT INTO post(id, title, body, author) VALUES (%s, %s, %s, %s) """ \
			,(post_id, title, body, author))

		self.con.commit()

		return self.get(post_id)

	def delete(self, post_id):
		query = """
			DELETE FROM post where id = %s;
		"""
		self.cursor.execute(query, (post_id,))

		return "deleted";


