from resources.repo.mysql import MySQLRepo
from backend.main import db

class DbChanges(MySQLRepo):
	def PersonChange(self):
		query = """
			 ALTER TABLE person ALTER COLUMN id VARCHAR(36);
		"""

		self.cursor.execute(query)
		self.con.commit()

		return "Person table updated"
		
