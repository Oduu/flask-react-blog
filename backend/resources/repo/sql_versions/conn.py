import pymysql

class DB:
	def mysql():
		connection = pymysql.connect(host="localhost", user='root', password="password", db="blog", charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
		return connection
