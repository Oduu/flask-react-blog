import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# why does this need to be imported to created Comment table but post table is created without
from resources.models.comment import Comment
from resources.models.person import Person

from resources.models.base import Base

engine = create_engine('mysql://root:password@localhost', echo=True, isolation_level="READ COMMITTED")
engine.execute("CREATE DATABASE IF NOT EXISTS blog") #create db if doesn't exist
engine.execute("USE blog") # select new db
	
Session = sessionmaker(bind=engine)
dbsession = Session()

print(Base.metadata.tables)
Base.metadata.create_all(engine)