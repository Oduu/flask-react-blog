from flask import Flask
from flask_restful import Api, Resource, reqparse, abort
from flask_cors import CORS

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from resources.routes.posts import Posts
from resources.routes.users import Users
from resources.routes.sessions import Sessions
from resources.routes.comments import Comments

from resources.models.base import Base

app = Flask(__name__)

# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://username:password@localhost/db_name'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:password@localhost/blog'
app.secret_key = "7h8[M~$@QR</+}["

api = Api(app)
cors = CORS(app, resources={r"/*/*": {"origins": "*"}})

api.add_resource(Posts, '/posts/<string:post_id>', '/posts')
api.add_resource(Users, '/users', '/users/<string:username>')
api.add_resource(Sessions, '/sessions', '/sessions/<string:username>')
api.add_resource(Comments, '/comments/<string:comment_id>', '/comments')

if __name__ == '__main__':
    app.run(debug=True)