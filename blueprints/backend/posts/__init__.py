from flask import Blueprint                            
from flask_restful import Api
                                                       
posts_bp = Blueprint('posts', __name__,) 
posts_api = Api(posts_bp)  
                                                                                     
from . import routes 