from flask_restful import Api, Resource, reqparse, abort
from . import posts_api

blog_put_args = reqparse.RequestParser()

# with required True, if not provided the help message will be returned instead of setting the value to null
blog_put_args.add_argument("post_id", type=int, help="Please send an id", required=True)
blog_put_args.add_argument("title", type=str, help="Please send a post title", required=True)
blog_put_args.add_argument("body", type=str, help="Please send a post body", required=True)
blog_put_args.add_argument("author", type=str, help="Please send an author", required=True)


result = { }


def abort_on_no_exist(post_id):
	if post_id not in result:
		abort(404, error="Could not find post with given id")

def abort_on_exist(post_id):
	if post_id in result:
		abort(409, error="Post already exists with that id")


class Posts(Resource):
	def get(self, post_id):
		abort_on_no_exist(post_id)
		return result[post_id]

	def put(self, post_id):
		abort_on_exist(post_id)
		args = blog_put_args.parse_args()
		result[post_id] = args
		return result[post_id], 201

	def delete(self, post_id):
		abort_on_no_exist(post_id)
		del result[post_id]
		return '', 204

posts_api.add_resource(Posts, '/posts/<int:post_id>')