from flask import Flask, Blueprint
from flask_restful import Api

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

def create_app(config_name):
   app = Flask(__name__)

   from .main import main_bp as main_blueprint
   from .posts import posts_bp as posts_blueprint

   app.register_blueprint(main_blueprint)
   app.register_blueprint(posts_blueprint)
   app.register_blueprint(api_bp)

   return app