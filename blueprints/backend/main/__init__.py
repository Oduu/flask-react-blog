from flask import Blueprint                            
from flask_restful import Api
                                                       
main_bp = Blueprint('main', __name__,) 
main_api = Api(main_bp)  
                                                                                     
from . import routes 