from flask_restful import Resource
from . import main_api

class Main(Resource):
    def get(self):
        return {1: "Main"}

main_api.add_resource(Main, '/')