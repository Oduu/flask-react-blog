import React from 'react';
import '../../css/layout/RightNav.css';
import {Link} from 'react-router-dom';

export default class RightNav extends React.Component {

    render() {

        const openInNewTab = (url) => {
            const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
            if (newWindow) newWindow.opener = null
        }

        return (
            <div className={(this.props.open ? 'flex' : 'hide')}>
                <ul id="link-list">

                    <Link to="/" onClick={this.props.toggleMenu}>
                        <li className="nav-link">Home</li>
                    </Link>

                    <Link to="/blog" onClick={this.props.toggleMenu}>
                        <li className="nav-link">Blog</li>
                    </Link>

                    <ul className="socials">
                        <i className="fa fa-twitter fa-2x" onClick={()=> openInNewTab("https://twitter.com/redding_harry")}></i>
                        <i className="fa fa-gitlab fa-2x" onClick={()=> openInNewTab("https://gitlab.com/Oduu")}></i>
                        <i className="fa fa-linkedin fa-2x" onClick={()=> openInNewTab("https://www.linkedin.com/in/henry-redding-86923115a")}></i>
                    </ul>

                </ul>
            </div>
        );
    }
}
