import React from 'react';
import './../css/Home.css';
import axios from 'axios';

export class Home extends React.Component {

	constructor(props) {
		super(props);
		this.handleUsername = this.handleUsername.bind(this);
		this.handlePassword = this.handlePassword.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	state = {
		username: "",
		password: ""
	}

	handleSubmit(e) {
		e.preventDefault();
	    axios.post(`http://localhost:5000/sessions`, {
	    	username: this.state.username,
	    	password: this.state.password
	    })
	      .then(res => {
	        	
	        	console.log(res)
	        
	      })
	}

	handleUsername(e) {
		this.setState({ username: e.target.value }, ()=> {
			console.log(this.state.username)
		})
	}

	handlePassword(e) {
		this.setState({ password: e.target.value }, ()=> {
			console.log(this.state.password)
		})
	}

	render() {

		const openInNewTab = (url) => {
    		const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
    		if (newWindow) newWindow.opener = null
		}

		return (
			<div className="Home">

				<div className="intro">

					<h2 id="title">Hello, I'm Harry and welcome to my blog</h2> 
					<p>Here I post some thoughts and guides on what I work on during my <u className="link" 
						onClick={()=> openInNewTab("https://gitlab.com/Oduu")}>software development projects.</u>
					</p>
					<p>I mainly work with <u className="bold"> PHP, Python or React</u> so most of the stuff here will revolve around that.</p>
					<p>Check out some of my most recent posts below or if you would like to contact me on a professional level please visit my <u className="link" 
						onClick={()=> openInNewTab("https://reddinghf.com/")}>portfolio site.</u>
					</p>

				</div>

			      <form onSubmit={this.handleSubmit}>
			        <label>
			          Name:
			          <input type="text" value={this.state.username} onChange={this.handleUsername} />        
			        </label>

					<label>
			          Password:
			          <input type="text" value={this.state.password} onChange={this.handlePassword} />        
			        </label>

			        <input type="submit" value="Submit" />
			      </form>

				<div className="recent-posts">

				</div>


				<div className="footer">


				</div>

{/*				<h2 id="title">Hosting a React App using Nginx</h2>

				<p id="publish-date">October 15, 2020 · by Harry Redding</p>

				<div className="prereq">
					<h4>Prerequisites</h4>
					<ul className="prereq-list">
						<li>Your chosen domain name (e.g. via GoDaddy)</li>
						<li>A server/droplet (e.g. via Digital Ocean)</li>
					</ul>
				</div>*/}

			</div>
		)
	}
}

export default Home;